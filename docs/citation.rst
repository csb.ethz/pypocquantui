Citing pyPOCQuant
======

If you find pyPOCQuant useful please cite our paper:

	Cuny, A. P., Rudolf, F., & Ponti, A. (2021). pyPOCQuant - A tool to 	automatically quantify Point-Of-Care Tests from images. SoftwareX,. 		https://doi.org/10.1016/j.softx.2021.100710},

or this repository using its DOI as follows:

	https://doi.org/10.1016/j.softx.2021.100710}

Note: this DOI will resolve to the first version of pyPOCQuant.

.. code-block:: bibtex

	@article{cuny2021,
	title = {pyPOCQuant — A tool to automatically quantify Point-Of-Care Tests from images},
	journal = {SoftwareX},
	volume = {15},
	pages = {100710},
	year = {2021},
	issn = {2352-7110},
	doi = {https://doi.org/10.1016/j.softx.2021.100710},
	url = {https://www.sciencedirect.com/science/article/pii/S2352711021000558},
	author = {Andreas P. Cuny and Fabian Rudolf and Aaron Ponti},
	keywords = {Lateral flow assay (LFA), Lateral flow immunoassay (LFIA), Point of care test (POCT), Test line quantification, Readout zone quantification, Diagnostics, Computer vision, QR code, Rapid testing, Rapid diagnostic tests (RTD)},
	abstract = {Lateral flow Point-Of-Care Tests (POCTs) are a valuable tool for rapidly detecting pathogens and the associated immune response in humans and animals. In the context of the SARS-CoV-2 pandemic, they offer rapid on-site diagnostics and can relieve centralized laboratory testing sites, thus freeing resources that can be focused on especially vulnerable groups. However, visual interpretation of the POCT test lines is subjective, error prone and only qualitative. Here we present pyPOCQuant, an open-source tool implemented in Python 3 that can robustly and reproducibly analyze POCTs from digital images and return an unbiased and quantitative measurement of the POCT test lines.}
	}

